﻿using myTunes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using myTunes.Helper;
using System.Collections;

namespace myTunes.Repositories
{
    public class MusicRepository
    {
        // List 5 random artists from database: Chinook
        public List<Artist> GetFiveRandomArtist()
        {
            List<Artist> artistList = new List<Artist>();
            //Sql query using newid() to get 5 random aritst
            string sql = "SELECT TOP(5) ArtistId, Name FROM Artist ORDER BY NEWID()";
            try
            {
                       //Open connection to database using connectionHelper
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        //Reads from database
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Results from query to Artist model
                                Artist artist = new Artist
                                {
                                    Name = reader.GetString(1)
                                };
                                artistList.Add(artist);
                            }
                        }
                    }
                }
            }
            //Catch Sql exception
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return artistList;
        }

        // List 5 random genres from database: Chinook
        public List<Genre> GetFiveRandomGenre()
        {
            List<Genre> genreList = new List<Genre>();
            //Sql query using newid() to get 5 random genres
            string sql = "SELECT TOP(5) GenreId, Name FROM Genre ORDER BY NEWID()";
            try
            {
                //Open connection to database using connectionHelper
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            //Results from query to Genre model
                            while (reader.Read())
                            {
                                Genre genre = new Genre
                                {
                                    Name = reader.GetString(1)
                                };
                                genreList.Add(genre);

                            }
                        }
                    }
                }
            }
            //Catch Sql exception
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return genreList;
        }

        // List 5 random tracks from database: Chinook
        public List<Track> GetFiveRandomTracks()
        {
            List<Track> trackList = new List<Track>();

            //Sql query using newid() to get 5 random genres
            string sql = "SELECT TOP(5) TrackId, Name FROM Track ORDER BY NEWID()";
            try
            {
                //Open connection to database using connectionHelper
                using SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring());
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            //Results from query to Track model
                            Track track = new Track
                            {
                                Name = reader.GetString(1)
                            };

                            trackList.Add(track);

                        }
                    }
                }
            }
            //Catch Sql exception
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return trackList;
        }

        //List tracksearch by (search) can be mutiple tracks with same name. 
        public List<TrackSearch> SearchTrack(String search)
        {
            List<TrackSearch> trackList = new List<TrackSearch>();

            string sql = "SELECT Track.TrackId, Track.Name, Artist.Name AS Artist, Album.Title, Genre.Name AS GenreName " +
                    "FROM Album " +
                    "INNER JOIN Track  ON Album.AlbumId = Track.AlbumId " +
                    "INNER JOIN Genre ON Track.GenreId = Genre.GenreId " +
                    "INNER JOIN Artist ON Album.ArtistId = Artist.ArtistId " +
                    "WHERE Track.Name = @name";
                                //sql query where Track.Name = input/search;
            try
            {
                //Open connection to database using connectionHelper
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {   //sql query add query with value from search
                        cmd.Parameters.AddWithValue("@name", search);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            //Results from query to Track model
                            while (reader.Read())
                            {
                                Genre genre = new Genre
                                {
                                    Name = reader.GetString(4)
                                };
                                Artist artist = new Artist
                                {
                                    Name = reader.GetString(2)
                                };
                                TrackSearch trackInfo = new TrackSearch
                                {
                                    Name = reader.GetString(1),
                                    Genre = genre,
                                    AlbumName = reader.GetString(3),
                                    Artist = artist
                                };
                                trackList.Add(trackInfo);
                            }
                        }
                    }
                }
            }
            //Catch Sql exception
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return trackList;
        }
      

    }
}
