﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;


namespace myTunes.Helper
{
    public class ConnectionHelper
    {
        public static string GetConnectionstring()
        {
            SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.DataSource = "DESKTOP-VS1QAQP\\SQLEXPRESS";
            connectionStringBuilder.InitialCatalog = "Chinook";
            connectionStringBuilder.IntegratedSecurity = true;
            return connectionStringBuilder.ConnectionString;
        }


        //Testing connection 
        public static void TestConnection()
        {
            //Build Connect string
            SqlConnectionStringBuilder connect = new SqlConnectionStringBuilder();

            connect.DataSource = "DESKTOP-VS1QAQP\\SQLEXPRESS";
            connect.InitialCatalog = "Chinook";
            connect.IntegratedSecurity = true;

            try
            {
                Console.WriteLine("Trying to connect");
                using (SqlConnection conn = new SqlConnection(connect.ConnectionString))
                {
                    conn.Open();
                    Console.WriteLine("Connection Sucessfull");
                }

            } catch(SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
