﻿using myTunes.Controllers;
using myTunes.Helper;
using myTunes.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace myTunes.Repositories
{
    public class CustomerRepository
    {
        // Get all customers from customer table
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                // Test if value is not null before assigning value.
                                // Some data in the db is incomplete
                                customer.CustomerId = reader.GetInt32(0);
                                if (!reader.IsDBNull(1))
                                    customer.FirstName = reader.GetString(1);
                                if (!reader.IsDBNull(2))
                                    customer.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3))
                                    customer.Country = reader.GetString(3);
                                if (!reader.IsDBNull(4))
                                    customer.PostalCode = reader.GetString(4);                
                                if (!reader.IsDBNull(5))
                                    customer.Phone = reader.GetString(5);
                                if (!reader.IsDBNull(6))
                                    customer.Email = reader.GetString(6);

                                customerList.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customerList;
        }
        // Returns customers most popular genre based on purchases
        public CustomerPopularGenre GetCustomerPopularGenre(int id)
        {
            CustomerPopularGenre customer = new CustomerPopularGenre();
            // Returns customers most popular genre. In the case of a tie, both are displayed
            string sql = "SELECT TOP 1 WITH TIES c.CustomerId, c.FirstName, c.LastName, g.Name as TopGenre, COUNT(g.Name) AS Purchases " +
                "FROM Customer as c, Invoice as i, InvoiceLine as il, Track as t, Genre as g WHERE c.CustomerId = @CustomerId AND " +
                "c.CustomerId = i.CustomerId AND i.InvoiceId = il.InvoiceId AND il.TrackId = t.TrackId AND t.GenreId = g.GenreId " +
                "GROUP BY c.CustomerId, c.FirstName, c.LastName, g.Name ORDER BY Purchases DESC";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);             
                                customer.PopularGenre.Add(reader.GetString(3));
                                customer.TotalPurchases = reader.GetInt32(4);   
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customer;
        }
        // Returns all customers and their invoice total. Descending order
        public List<CustomerTotal> GetCustomerTotal()
        {
            List<CustomerTotal> totalList = new List<CustomerTotal>();
       
           string sql = "SELECT c.FirstName, c.LastName, SUM(Total) as TotalAmount FROM Customer AS c, Invoice as i " +
                "WHERE c.CustomerId = i.CustomerId GROUP BY i.CustomerId, c.FirstName, c.LastName ORDER BY TotalAmount DESC";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerTotal customer = new CustomerTotal();

                                customer.FirstName = reader.GetString(0);
                                customer.LastName = reader.GetString(1);
                                customer.TotalAmount = reader.GetDecimal(2);

                                totalList.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return totalList;

        }
        // Returns number of customers in each country
        public List<CustomersPerCountry> GetCustomersPerCountry()
        {
            List<CustomersPerCountry> perCountryList = new List<CustomersPerCountry>();
            string sql = "SELECT Country, Count(*) as NumberOfCustomers FROM Customer GROUP BY Country ORDER BY NumberOfCustomers DESC";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomersPerCountry temp = new CustomersPerCountry();

                                temp.Country = reader.GetString(0);
                                temp.NumberOfCustomers = reader.GetInt32(1);
                            
                                perCountryList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return perCountryList;
        }
        // Returns a single customer
        public Customer GetCustomer(int id)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer " +
                "WHERE CustomerId = @CustomerId";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                if (!reader.IsDBNull(1))
                                    customer.FirstName = reader.GetString(1);
                                if (!reader.IsDBNull(2))
                                    customer.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3))
                                    customer.Country = reader.GetString(3);
                                if (!reader.IsDBNull(4))
                                    customer.PostalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5))
                                    customer.Phone = reader.GetString(5);
                                if (!reader.IsDBNull(6))
                                    customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customer;
        }
        // Adds a customer to the customer table
        public bool AddCustomer(Customer customer)
        {
            bool success = false;

            string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return success;
        }
        // Updates an existing customer
        public bool UpdateCustomer(Customer customer)
        {
            bool success = false; // Currently no success message is returned. -> customercontroller.

            string sql = "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName, Country = @Country, " +
                "PostalCode = @PostalCode, Phone = @Phone, Email = @Email WHERE CustomerId = @CustomerId";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return success;
        }
    }
}
