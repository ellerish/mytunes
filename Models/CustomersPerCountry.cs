﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myTunes.Models
{
    public class CustomersPerCountry
    {
        public string Country { get; set; }
        public int NumberOfCustomers { get; set; }
    }
}
