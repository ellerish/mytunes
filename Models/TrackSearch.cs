﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myTunes.Models
{
    //Model TrackSearch
    public class TrackSearch
    {
        public string Name { get; set; }
        public string AlbumName { get; set; }
        public Artist Artist { get; set; }
        public Genre Genre { get; set; }
    }
}
