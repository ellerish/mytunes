﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myTunes.Models
{
    public class CustomerPopularGenre
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int TotalPurchases { get; set; }
        public List<string> PopularGenre { get; set; } = new List<string>();
    }
}
