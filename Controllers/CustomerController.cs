﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using myTunes.Models;
using myTunes.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace myTunes.Controllers
{
    [Route("api/customers")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly CustomerRepository _customerRepository;

        public CustomerController(CustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        // GET: api/customers
        [HttpGet]
        public ActionResult<IEnumerable<Customer>> Get()
        {
            // Going to use our CustomerRepository to fetch all customers
            return Ok(_customerRepository.GetAllCustomers());
        }

        // GET api/customers/5
        [HttpGet("{id}")]
        public ActionResult<Customer> Get(int id)
        {
            // Going to use our CustomerRepository to fetch a specific customer
            return Ok(_customerRepository.GetCustomer(id));
        }

        // GET api/customers/per-country
        [HttpGet("per-country")]
        public ActionResult<IEnumerable<CustomersPerCountry>> GetCountry() 
        {
            return Ok(_customerRepository.GetCustomersPerCountry());
        }

        // GET api/customers/total-invoice
        [HttpGet("total-invoice")]
        public ActionResult<IEnumerable<CustomerTotal>> GetTotal()
        {
            // Fetch all customers and their total invoice
            return Ok(_customerRepository.GetCustomerTotal());
        }

        // GET api/customers/5/popular/genre
        [HttpGet("{id}/popular/genre")]
        public ActionResult<IEnumerable<CustomerPopularGenre>> GetPopularGenre(int id)
        {
            // Going to use our CustomerRepository to fetch a specific customers most popular genre
            return Ok(_customerRepository.GetCustomerPopularGenre(id));
        }

        // POST api/customers
        [HttpPost]
        public ActionResult Post(Customer customer)
        {
            // Going to use our CustomerRepository to add a new customer
            bool success = _customerRepository.AddCustomer(customer);
            return CreatedAtAction(nameof(Get), new { id = customer }, success);
        }

        // PUT api/customers/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, Customer customer)
        {
            // Check for bad request
            if (id != customer.CustomerId)
            {
                return BadRequest();
            }
            // Going to use our CustomerRepository to update an existing customer
            bool success = _customerRepository.UpdateCustomer(customer);
            return NoContent();
        }
    }
}