﻿using Microsoft.AspNetCore.Mvc;
using myTunes.Models;
using myTunes.Repositories;
using System;
using System.Collections;
using System.Collections.Generic;

namespace myTunes.Controllers
{
    [Route("api/music")]
    [ApiController]
    public class MusicController : ControllerBase
    {
        private readonly MusicRepository _musicRepository;

        public MusicController(MusicRepository musicRepository)
        {
            _musicRepository = musicRepository;
        }
        // GET: api/music/samples
        [Route("samples")]
        [HttpGet]
        public ActionResult GetSampleMusic()
        {    
            // Going to use our MusicRepository to fetch five random artists, genres, tracks
            List<Track> Tracks = _musicRepository.GetFiveRandomTracks();
            List<Artist> Artists = _musicRepository.GetFiveRandomArtist();
            List<Genre> Genres = _musicRepository.GetFiveRandomGenre();
               return Ok(new { Tracks, Artists, Genres });
        }

        // GET: api/music/samples/artists 
        [Route("samples/artists")]
        [HttpGet]
        public ActionResult<IEnumerable<Artist>> GetFiveRandomArtist()
        {
           // Going to use our MusicRepository to fetch five random artist 
            return Ok(_musicRepository.GetFiveRandomArtist());
     
        }

        // GET: api/music/samples/tracks 
        [Route("samples/tracks")]
        [HttpGet]
        public ActionResult<IEnumerable<Track>> GetFiveRandomTracks()
        {
            // Going to use our MusicRepository to fetch five random tracks 
            return Ok(_musicRepository.GetFiveRandomTracks());
        }

        // GET: api/music/samples/genres 
        [Route("samples/genres")]
        [HttpGet]
        public  ActionResult<IEnumerable<Genre>> GetFiveRandomGenre()
        {
            // Going to use our MusicRepository to fetch five random genre 
            return Ok(_musicRepository.GetFiveRandomGenre());
        }

       // GET api/music/search/tracks=foo
        [Route("search/track={name}")]
        [HttpGet]
        public ActionResult<Track> GetBySearch(string name)
        {
            // Going to use our musicRepository to fecth track by name
            return Ok(_musicRepository.SearchTrack(name));
        }

    }
}
