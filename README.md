# myTunes
 ASP.NET Core Web Application (API) that will show songs and manage customers.
 REST endpoints with ASP.NET.
 SqlClient to extract information from a database and expose it through an MVC application.
 The database used for this project is the sample database Chinook.

## Table of Contents
- [Features](#features)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)

## Features

### Models:
- POCO classes with properties, to represent each model

### Controllers:
- Exposes methods that will be called when the API receives requests through the route.
    - MusicController to handle request/responses for musicAPI.
    - CustomerController to handle request/responses for customersAPI.

### Repositories
- Customer queries:
    - Get all customers
    - Get a specific customer
    - Add a new customer
    - Update existing cutomer
    - Get the number of costumers in each country
    - Get customers who are the highest spenders in descending order
    - Get a customer's top genre based on invoices
- Music queries
    - Get 5 random artists
    - Get 5 random tracks
    - Get 5 random genre
    - Search for track in database

### Postman:
- Customer: Postman collection for testing endpoints for Customer API
- Music: Postman collection for testing endpoints for Music API

## Install
- Clone 
- Open in Visual Studio
- Go to Repositories/ConnectionHelper.cs -> Change DataSource in GetConnectionString() to your own db connection.
- Open SSMS and run script to create the sample database Chinook.
    - Script can be found here: https://github.com/lerocha/chinook-database/blob/master/ChinookDatabase/DataSources/Chinook_SqlServer.sql  
- Build the application

## Usage
- Run the application from Visual Studio.
- Test endpoints in browser/Postman

## Maintainers
- Melfyn Jenkins
- Elise Rishaug
